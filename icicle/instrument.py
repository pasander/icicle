'''
Instrument interface and associated decorator tools. 

This module contains the `Instrument` and `Singleton` interface and metaclass, as well as the InstrumentTimeoutError exception and LOCK_NOT_ACQUIRED dummy class. 

The `@acquire_lock` and `retry_on_fail` decorators are also provided here.
'''

from functools import wraps
from threading import Lock
import importlib
import math
import numpy
import time
import logging

logger = logging.getLogger(__name__)

class Singleton(type):
    '''
    Singleton class provides global instance access to instruments/instrument types. `_instances` member is dict of instruments by class name.

    .. Note: This is not optimal - should change to Multiton based on resource address to ensure resource addresses are not duplicated, rather than limiting number of instruments of same class!!

    .. Warning: Deprecated - this is not in use any more!

    '''

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class Multiton(type):
    '''
    Multiton metaclass provides global instance access to instruments/instrument types based off first argument in constructor (a bit silly, but I can't think of a way around this at the moment). `_instances` member is dict of instruments by key.

    .. Warning: All arguments must be provided as keyword arguments when subclass is init'd - this masks the "defaults" defined in the metaclass'd class. 

    '''

    _instances = {}

    def __init__(cls, name, bases, namespace, **kwargs):
        cls.__key = kwargs['key']

    def __new__(metacls, name, bases, namespace, key='resource'):
        return super().__new__(metacls, name, bases, namespace)

    def __call__(cls, *args, **kwargs):
        key = kwargs[cls.__key]
        if key not in cls._instances:
            cls._instances[key] = super(Multiton, cls).__call__(*args, **kwargs)
        return cls._instances[key]

    def get_instance(cls, key=None):
        if key is not None:
            if key not in cls._instances or not isinstance(cls._instances[key], cls):
                raise KeyError(f'Instrument of class {cls} with key {key} not found for get_instance(...) call - has it been initialised yet?')
            return cls._instances[key]
        else:
            return tuple(value for value in cls._instances.values() if isinstance(value, cls))


class InstrumentTimeoutError(Exception):
    '''
    Exception to be throw when instruments time out. 
    '''

    def __init__(function, instrument_class, message=''):
        self.__init__(self.message)
        self.function = function
        self.instrument_class = instrument_class

class LOCK_NOT_ACQUIRED:
    '''
    Dummy enum-like class to represent return value when lock could not be acquired by `acquire_lock()`.
    '''
    pass

def acquire_lock(try_once = False, timeout = -1):
    '''
    Instrument Member Functon Decorator that attempts to obtain a global mutex on the singleton class of the function this decorator decorated. 

    Can be disabled by passing keyword argument `no_lock=True` in a call to the decorated function.

    :param try_once: If true, attempt to get lock doesn't block. Returns `LOCK_NOT_ACQUIRED` on failure if true, continuously retries if false.
    :param timeout: Number of seconds to retry for until timeout. If = -1, infinite.

    :return: Wrapped function.

    .. note:: DECORATOR
    '''
    def _acquire_lock(function):
        @wraps(function)
        def wrapper(self, *args, **kwargs):
            logger.debug(f'Checking lock requirements for call to {function.__name__}...')
            acquired = False
            needs_release = False
            try_once_ = try_once
            timeout_ = timeout
            
            # Parse kwargs
            if 'try_once' in kwargs:
                try_once_ = kwargs.pop('try_once')
            if 'timeout' in kwargs:
                timeout_ = kwargs.pop('timeout')
            if 'no_lock' in kwargs:
                logger.debug(f'Lock bypassed...')
                acquired = kwargs.pop('no_lock')

            # Try to acquire lock, or ignore if no_lock bypass passed
            if not acquired:
                logger.debug(f'Attempting to acquire lock for call to {function.__name__}...')
                acquired = self._lock.acquire(blocking=(not try_once_), timeout = timeout_)
                needs_release = True

            if acquired:
                if needs_release:
                    logger.debug(f'Lock acquired \\/')
                try:
                    rval = function(self, *args, **kwargs)
                finally:
                    if needs_release:
                        logger.debug(f'Lock released /\\')
                        self._lock.release()
            else:
                if try_once:
                    ret = LOCK_NOT_ACQUIRED
                else:
                    raise InstrumentTimeoutError(function, type(self), f'Timeout on instrument of class {type(self).__name__} executing function {function.__name__}.')
            return rval
        return wrapper
    return _acquire_lock
# end def acquire_lock()

def retry_on_fail(attempts = 3, exceptions = []):
    '''
    Instrument Member Functon Decorator that attempts to retry the given instrument member function on failure, after reconnecting to instrument, if required (via context closure and re-establishment).

    :param attempts: Number of times to retry function call. 
    :param exceptions: Exception classes to catch and ignore.

    :return: Wrapped function.

    .. note:: DECORATOR
    '''
    def _retry_on_fail(function):
        @wraps(function)
        def wrapper(self, *args, **kwargs):
            acquired = False

            attempts_ = attempts - 1
            # Parse kwargs
            if 'attempts' in kwargs:
                attempts_ = kwargs.pop('attempts') - 1
            max_attempts = attempts_

            if not self._connected:
                raise RuntimeError(f'Function {function.__name__} on instrument {type(self).__name__} requires active connection - please make sure to call __enter__() via with ...: or similar.')
            
            # Try to run function, catch exceptions
            logger.debug(f'Trying {function.__name__} up to {max_attempts + 1} times...')
            while True:
                logger.debug(f'Attempt {max_attempts - attempts_ + 1}/{max_attempts + 1}...')
                try:
                    return function(self, *args, **kwargs)
                except Exception as e:
                    logger.warn(f'Failure... try to renew connection.')
                    if type(e) not in exceptions or attempts_ == 0:
                        raise e
                    else:
                        # Try to re-establish context
                        self.__exit__(recover_attempt=True, no_lock=True)
                        self.__enter__(recover_attempt=True, no_lock=True)
                attempts_ -= 1
            #raise RuntimeError(f'Max attempts exceeded in function {function.__name__} on instrument {type(self).__name__}')
        return wrapper
    return _retry_on_fail
# end def retry_on_fail()

class Instrument():
    '''
    Base Instrument class - all instruments should derive from this. Will be singleton'd at implementation level.

    Implements the global lock for each instrument to ensure only one instruction is executed at a time.

    Tracks connected/disconnected state. Connection occurs within the context handler (see an implementation).
    '''

    def __init__(self):
        '''
        Instrument Constructor. Constructs lock.
        '''
        self._lock = Lock()
        self._connected = False
        pass

    @acquire_lock()
    def __enter__(self, recover_attempt = False):
        '''
        Must establish connection to instrument.

        :param recovery_attempt: Whether this is a recovery attempt by `retry_on_fail`. Should be propagated.

        :returns: Instrument object with activated context.
        '''
        self._connected = True
        return self

    @acquire_lock()
    def __exit__(self, exception_type = None, exception_value = None, traceback = None, recover_attempt = False):
        '''
        Must gracefully (or otherwise) terminate connection with instrument.

        :param recovery_attempt: Whether this is a recovery attempt by `retry_on_fail`. Should be propagated.
        '''
        self._connected = False
        pass

    def connected(self):
        return self._connected
    
    @acquire_lock()
    @retry_on_fail()
    def reset(self, *args, **kwargs):
        '''
        RESET template - should be implemented to reset device.
        '''
        raise NotImplementedError('Instrument class is not a full implementation. Please use appropriate subclass and make sure on() is implemented.')

    @acquire_lock()
    @retry_on_fail()
    def on(self, *args, **kwargs):
        '''
        ON template - should be implemented to turn on device or channel.
        '''
        raise NotImplementedError('Instrument class is not a full implementation. Please use appropriate subclass and make sure on() is implemented.')

    @acquire_lock()
    @retry_on_fail()
    def off(self, *args, **kwargs):
        '''
        OFF template - should be implemented to turn off device or channel.
        '''
        raise NotImplementedError('Instrument class is not a full implementation. Please use appropriate subclass and make sure off() is implemented.')

    @acquire_lock()
    @retry_on_fail()
    def status(self, *args, **kwargs):
        '''
        STATUS template - should be implemented to return device/channel status.
        '''
        raise NotImplementedError('Instrument class is not a full implementation. Please use appropriate subclass and make sure status() is implemented.')

    @acquire_lock()
    @retry_on_fail()
    def set(self, *args, **kwargs):
        '''
        SET template - should be implemented to set and query back device state.
        '''
        raise NotImplementedError('Instrument class is not a full implementation. Please use appropriate subclass and make sure set() is implemented.')

    @acquire_lock()
    @retry_on_fail()
    def query(self, *args, **kwargs):
        '''
        QUERY template - should be implemented to query device state.
        '''
        raise NotImplementedError('Instrument class is not a full implementation. Please use appropriate subclass and make sure query() is implemented.')

    @acquire_lock()
    @retry_on_fail()
    def measure(self, *args, **kwargs):
        '''
        MEASURE template - should be implemented to make measurement on device.
        '''
        raise NotImplementedError('Instrument class is not a full implementation. Please use appropriate subclass and make sure measure() is implemented.')

    def open(self):
        self.__enter__()

    def close():
        self.__exit__()

    @acquire_lock()
    @retry_on_fail()
    def sweep(self, target_setting, target_value, delay = 1, step_size = None, n_steps = None, measure = False, set_function = None, set_args = {}, measure_function = None, measure_args = {}, query_args = {}, conversion = lambda k: float(k), log_function=None, execute_each_step=None):
        '''
        Device-independent implementation of a parameter sweep.

        .. warning: Log_function header printing is currently hacked to work with Keithley2410 (only) I think for nice CLI output... this should be fixed!

        :param target_setting: name of setting that `set()` call should target.
        :param target_value: value of setting to sweep to (from current).
        :param delay: delay between sweep steps (in seconds)
        :param step_size: step size between sweep steps
        :param n_steps: number of steps to perform (alternative to step_size - specify one but not both)
        :param set_function: function to use for `set()` call. Defaults to `self.set`
        :param set_args: additional keyword arguments to pass to `set()`
        :param measure: whether to measure at each step of sweep.
        :param measure_function: function to use for `measure()` call. Defaults to `self.measure`
        :param measure_args: additional keyword arguments to pass to `measure()`
        :param query_args: additional keyword arguments to pass to `query()` at each step.
        :param conversion: function to convert query return value back to float.
        :param execute_each_step: accepts a callback function (no arguments) to be executed at each step of the sweep.

        :return: final parameter value[, measure data]

        '''
        SPACING = 17
        current_value = conversion(self.query(target_setting, **query_args, no_lock=True))

        if set_function == None:
            set_function = self.set
        if measure_function == None:
            measure_function = self.measure

        # Already there
        if abs(current_value - target_value) < 1e-12:
            return current_value

        #if step_size is None and n_steps is not None:
        #    step_size = float(target_value - current_value) / n_steps
        if n_steps is None and step_size is not None:
            n_steps = math.ceil(abs(float(target_value - current_value) / step_size))
        else: 
            raise RuntimeError('Exactly one of step_size and n_steps must be provided - not neither or both!')
        # Enforce integer n_steps
        n_steps = int(math.floor(n_steps))
        if log_function:
            log_function(f'# Will sweep {target_setting} to {target_value} in {n_steps} steps of size {step_size}')
	
        if log_function:
            # Hack for Keithley2410 CLI - fix this!!!
            log_function('\t'.join(val.ljust(SPACING) for val in ('# Time', 'Voltage (V)', 'Current (A)', 'Resistance', 'PSU Time', 'Status Register')))

        # Data collection
        if measure or log_function:
            data = []
        #    data.append((time.time(), *self.measure(**measure_args, no_lock = True)))
        #if log_function:
        #    log_function('\t'.join([time.strftime('%x %X').ljust(SPACING), *[str(val).ljust(SPACING) for val in data[-1][1:]]]))

        # Sweep target:
        for intermediate_value in numpy.linspace(current_value, target_value, num=n_steps+1):
            if execute_each_step:
                execute_each_step()
            self.set(target_setting, intermediate_value, no_lock=True, **set_args)
            time.sleep(delay)
            if measure or log_function:
                data.append((time.time(), *measure_function(**measure_args, no_lock = True)))
            if log_function:
                log_function('\t'.join([time.strftime('%x %X').ljust(SPACING), *[str(val).ljust(SPACING) for val in data[-1][1:]]]))
        
        final_value = conversion(self.query(target_setting, **query_args, no_lock=True))

        if measure:
            return final_value, data
        else:
            return final_value

    @staticmethod
    def load_instrument_class(instrument, instrument_class):
        '''
        Static function that finds an instrument class and loads it based on the module name and class name.

        :param instrument: Name of instrument module (e.g. `keithley2410`)
        :param instrument_class: Class of instrument (e.g. `Keithley2410`)
        :return: instrument class
        '''
        return getattr(importlib.import_module(f'.{instrument}', package='.'.join(__name__.split('.')[:-1])), instrument_class)

# end class Instrument
