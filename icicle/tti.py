'''
TTI class for TTI MX100TP-style low voltage power supplies.
'''
import argparse
import click
import time
import logging
from functools import update_wrapper

from .instrument import Singleton, acquire_lock
from .visa_instrument import retry_on_fail_visa
from .scpi_instrument import SCPIInstrument, is_in, is_integer, is_numeric, truthy

logger = logging.getLogger(__name__)

class TTI(SCPIInstrument, key='resource'):
    '''
    SCPIInstrument implementation for TTI MX100TP or similar low voltage power supply.
    '''

    TIMEOUT = 10000 # 10 seconds
    ''' Serial link timeout (ms). '''

    COM_RESET = '*RST; STATUS:PRESET; *CLS'
    ''' Instrument Reset SCPI command. '''

    SETTINGS = {
        'IDENTIFIER': {
            'QUERY': '*IDN?'
        },
        'OUTPUT1': {
            'SET': 'OP1 {}',
            'QUERY': 'OP1?',
            'verifier': truthy(true_output=1, false_output=0)
        },
        'OUTPUT2': {
            'SET': 'OP2 {}',
            'QUERY': 'OP2?',
            'verifier': truthy(true_output=1, false_output=0)
        },
        'OUTPUT3': {
            'SET': 'OP3 {}',
            'QUERY': 'OP3?',
            'verifier': truthy(true_output=1, false_output=0)
        },
        'OUTPUTALL': {
            'SET': 'OPALL {}',
            'verifier': truthy(true_output=1, false_output=0)
        },
        'VRANGE1': {
            'SET': 'VRANGE1 {}',
            'QUERY': 'VRANGE1?',
            'verifier': is_in(1, 2, 3, 4, 5, 6, 7, '1', '2', '3', '4', '5', '6', '7')
            # Output1: 1 = 30V/6A, 2 = 15V/10A, 3 = 60V/3A, 4 = 30V/12A, 5 = 15V/20A, 6 = 60V/6A, 7 = 120V/3A.
        },
        'VRANGE2': {
            'SET': 'VRANGE2 {}',
            'QUERY': 'VRANGE2?',
            'verifier': is_in(1, 2, 3, '1', '2', '3')
            # Output2: 1 = 30V/6A, 2 = 15V/10A, 3 = 60V/3A. 
        },
        'VRANGE3': {
            'SET': 'VRANGE3 {}',
            'QUERY': 'VRANGE3?',
            'verifier': is_in(1, 2, '1', '2')
            # Output3: 1 = 5.5V/3A, 2 = 12V/1.5A.
        },
        'OVP1': {
            'SET': 'OVP1 {}',
            'QUERY': 'OVP1?',
            'verifier': is_numeric()
        },
        'OVP2': {
            'SET': 'OVP2 {}',
            'QUERY': 'OVP2?',
            'verifier': is_numeric()
        },
        'OVP3': {
            'SET': 'OVP3 {}',
            'QUERY': 'OVP3?',
            'verifier': is_numeric()
        },
        'OCP1': {
            'SET': 'OCP1 {}',
            'QUERY': 'OCP1?',
            'verifier': is_numeric()
        },
        'OCP2': {
            'SET': 'OCP2 {}',
            'QUERY': 'OCP2?',
            'verifier': is_numeric()
        },
        'OCP3': {
            'SET': 'OCP3 {}',
            'QUERY': 'OCP3?',
            'verifier': is_numeric()
        },
        'DAMPING1': {
            'SET': 'DAMPING1 {}',
            'QUERY': 'DAMPING1?',
            'verifier': is_in('ON', 'OFF', 'LOW', 'MED', 'HIGH')
        },
        'DAMPING2': {
            'SET': 'DAMPING2 {}',
            'QUERY': 'DAMPING2?',
            'verifier': is_in('ON', 'OFF', 'LOW', 'MED', 'HIGH')
        },
        'DAMPING3': {
            'SET': 'DAMPING3 {}',
            'QUERY': 'DAMPING3?',
            'verifier': is_in('ON', 'OFF', 'LOW', 'MED', 'HIGH')
        },
        'VOLTAGE1': {
            'SET': 'V1 {}',
            'QUERY': 'V1?',
            'verifier': is_numeric()
        },
        'VOLTAGE2': {
            'SET': 'V2 {}',
            'QUERY': 'V2?',
            'verifier': is_numeric()
        },
        'VOLTAGE3': {
            'SET': 'V3 {}',
            'QUERY': 'V3?',
            'verifier': is_numeric()
        },
        'OUTPUT_VOLTAGE1': {
            'QUERY': 'V1O?'
        },
        'OUTPUT_VOLTAGE2': {
            'QUERY': 'V2O?'
        },
        'OUTPUT_VOLTAGE3': {
            'QUERY': 'V3O?'
        },
        'CURRENT1': {
            'SET': 'I1 {}',
            'QUERY': 'I1?',
            'verifier': is_numeric()
        },
        'CURRENT2': {
            'SET': 'I2 {}',
            'QUERY': 'I2?',
            'verifier': is_numeric()
        },
        'CURRENT3': {
            'SET': 'I3 {}',
            'QUERY': 'I3?',
            'verifier': is_numeric()
        },
        'OUTPUT_CURRENT1': {
            'QUERY': 'I1O?'
        },
        'OUTPUT_CURRENT2': {
            'QUERY': 'I2O?'
        },
        'OUTPUT_CURRENT3': {
            'QUERY': 'I3O?'
        },
        'VOLTAGE_STEP1': {
            'SET': 'DELTAV1',
            'QUERY': 'DELTAV1?',
            'verifier': is_numeric()
        },
        'VOLTAGE_STEP2': {
            'SET': 'DELTAV2',
            'QUERY': 'DELTAV2?',
            'verifier': is_numeric()
        },
        'VOLTAGE_STEP3': {
            'SET': 'DELTAV3',
            'QUERY': 'DELTAV3?',
            'verifier': is_numeric()
        },
        'CURRENT_STEP1': {
            'SET': 'DELTAI1',
            'QUERY': 'DELTAI1?',
            'verifier': is_numeric()
        },
        'CURRENT_STEP2': {
            'SET': 'DELTAI2',
            'QUERY': 'DELTAI2?',
            'verifier': is_numeric()
        },
        'CURRENT_STEP3': {
            'SET': 'DELTAI3',
            'QUERY': 'DELTAI3?',
            'verifier': is_numeric()
        }
    }
    ''' Settings dictionary with all Set/Query SCPI combinations. '''


    def __init__(self, resource = 'ASRL/dev/ttyACM0::INSTR', reset_on_init = True, off_on_close=False, outputs=3):
        '''
        .. Warning: the ``resource`` keyword argument is mandatory and must be explicitly specified - failing to do so will result in an error since the Multiton metaclass on VisaInstrument masks this default value for ``resource``.

        :param resource: VISA Resource address. See VISA docs for more info.
        :param reset_on_init: Whether to reset TTI using COM_RESET command on device initialisation.
        :param off_on_close: Whether to turn off outputs on __exit__.
        :param outputs: How many outputs this TTI power supply has.
        '''
        super().__init__(resource, reset_on_init)
        self._off_on_close = off_on_close
        self._outputs = outputs

    @property
    def outputs(self):
        '''
        :returns: Iterator over all outputs (i.e. numbers 1, 2, ...)
        '''
        return range(1, self._outputs + 1)

    def validate_channel(self, channel, raise_exception=True):
        '''
        Check if an output exists on this device.

        :param channel: Channel number to validate as an output.
        '''
        if channel >= 1 and channel <= self._outputs:
            return True
        else:
            if raise_exception:
                raise RuntimeError(f'Channel {channel} does not exist or is not enabled on this device.')
            else:
                return False

    @acquire_lock()
    def __enter__(self, recover_attempt = False):
        '''
        Initialises connection to TTI.

        :param recovery_attempt: Whether this is a recovery attempt by retry_on_fail.

        :return: `TTI` object in activated state.
        '''
        super().__enter__(recover_attempt=recover_attempt, no_lock = True)
        if self._reset_on_init and not recover_attempt:
            if any(self.status(i) != 0 for i in self.outputs):
                raise RuntimeError('Output on for TTI, but reset_on_init set. Dangerous - refusing to connect.')
            # Set default values here
            pass
        return self

    @acquire_lock()
    def __exit__(self, exception_type = None, exception_value = None, traceback = None, recover_attempt = False):
        '''
        Closes connection to TTI.

        :param recovery_attempt: Whether this is a recovery attempt by retry_on_fail.
        '''
        if self._off_on_close and not recover_attempt:
            # Should be safe for pretty much anything
            for output in self.outputs:
                self.off(output, no_lock=True)
        super().__exit__(exception_type, exception_value, traceback, no_lock=True)

    def set_channel(self, setting, channel, value, **kwargs):
        '''
        Set `setting` on instrument to `value` for output `channel`, and read-back using equivalent `query()`.

        :param setting: key in class dictionary SETTINGS.
        :param channel: channel to set this setting on.
        :param value: target value for `setting`.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :return: read-back value if `query()` available, else whether number of bytes written during `set()` meets expectation
        '''
        # uses lock from sub-call
        if channel != 'ALL':
            self.validate_channel(int(channel))
        return self.set(f'{setting}{channel}', value, **kwargs)

    def query_channel(self, setting, channel, **kwargs):
        '''
        Query `setting` on instrument for output `channel`.

        .. warning: Modifies currently selected `INSTRUMENT` on HMP4040 interface - this change persists.

        :param setting: key in class dictionary SETTINGS.
        :param channel: output to set this setting on.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :return: data returned by device for given query.
        '''
        # uses lock from sub-call
        self.validate_channel(int(channel))
        return self.query(f'{setting}{channel}', **kwargs)

    def off(self, channel, **kwargs):
        '''
        Turn off output.

        :param channel: output to be turned off.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.
        
        :return: read-back value.
        '''
        # uses lock from sub-call
        if channel != 'ALL':
            self.validate_channel(int(channel))
        return self.set_channel('OUTPUT', channel, 0, **kwargs)
    
    @acquire_lock()
    def on(self, channel):
        '''
        Turn on output. For some reason needs an explicit check/retry loop since this sometimes fails?

        :param channel: output to be turned on.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        
        :return: read-back value.
        '''
        if channel == 'ALL':
            ret = self.set_channel('OUTPUT', channel, 1, no_lock=True)
            time.sleep(0.1)
            count = 0
            while 0 in [self.status(ch, no_lock=True) for ch in self.outputs] and count < 3:
                for ch in self.outputs:
                    if self.status(ch, no_lock=True) == 0:
                        logger.error(f'Tried to turn LV CHANNEL {ch} on but is still off! Retrying...')
                ret = self.set_channel('OUTPUT', channel, 1, no_lock=True)
                count += 1
        else:
            self.validate_channel(int(channel))
            ret = self.set_channel('OUTPUT', int(channel), 1, no_lock=True)
            time.sleep(0.1)
            count = 0
            while self.status(int(channel), no_lock=True) == 0 and count < 3:
                logger.error(f'Tried to turn LV CHANNEL {channel} on but is still off! Retrying...')
                ret = self.set_channel('OUTPUT', int(channel), 1, no_lock=True)
                count += 1
        return ret

    def status(self, channel, **kwargs):
        '''
        Check status of output.

        :param channel: output to be turned on.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.
        
        :return: status of output.
        '''
        # uses lock from sub-call
        self.validate_channel(channel)
        return int(self.query_channel('OUTPUT', channel, **kwargs))
    
    @acquire_lock()
    def measure(self, channel):
        '''
        Measure output voltage, current.

        :param channel: output to be measured.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        
        :return: tuple `(voltage, current)`.
        '''
        return (
            float(self.query_channel('OUTPUT_VOLTAGE', channel, no_lock=True).strip().replace('V', '')),
            float(self.query_channel('OUTPUT_CURRENT', channel, no_lock=True).strip().replace('A', ''))
        )

    @acquire_lock()
    def _power_cycle(self, channel, delay):
        '''
        Callback function that can be used with `sweep()`.
        
        :param channel: output to be cycled.
        :param delay: delay during cycle (between off and on).

        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        '''
        self.validate_channel(channel)
        self.off(channel, no_lock=True)
        time.sleep(delay)
        self.on(channel, no_lock=True)
    
    def sweep(self, target_setting, channel, target_value, delay = 1, step_size = None, n_steps = None, measure = False, set_function = None, set_args = {}, measure_function = None, measure_args = {}, query_args = {}, log_function=None, power_cycle_each_step=False, **kwargs):
        '''
        Sweep `target_setting` on a given `channel`.

        :param target_setting: name of setting that `set()` call should target.
        :param channel: output to perform sweep on.
        :param target_value: value of setting to sweep to (from current).
        :param delay: delay between sweep steps (in seconds)
        :param step_size: step size between sweep steps
        :param n_steps: number of steps to perform (alternative to step_size - specify one but not both)
        :param set_function: function to use for `set()` call. Defaults to `self.set`
        :param set_args: additional keyword arguments to pass to `set()`
        :param measure: whether to measure at each step of sweep.
        :param measure_function: function to use for `measure()` call. Defaults to `self.measure`
        :param measure_args: additional keyword arguments to pass to `measure()`
        :param query_args: additional keyword arguments to pass to `query()` at each step.
        :param power_cycle_each_step: set callback function to `_power_cycle` with correct arguments.
        :param execute_each_step: accepts a callback function (no arguments) to be executed at each step of the sweep.

        :return: final parameter value[, measure data]
        '''
        # uses lock from sub-call
        self.validate_channel(int(channel))
        execute_each_step = None
        if power_cycle_each_step:
            execute_each_step = lambda: self._power_cycle(channel, delay, no_lock=True)
        return super().sweep(f'{target_setting}{channel}', target_value, delay=delay, step_size=step_size, n_steps=n_steps, measure=measure, set_function=set_function, set_args=set_args, measure_function=measure_function, measure_args=measure_args, conversion=lambda k: float(k.replace(f'I{channel}', '').replace(f'V{channel}', '').strip()), log_function=log_function, execute_each_step=execute_each_step, **kwargs)

    def publish(self, client, line, channel='ALL'):
        '''
        Publish data line (from Monitoring) to MQTT.

        TODO: don't reparse line here; instead pass/access raw data? 

        :param client: MQTTClient object to publish with.
        :param line: MonitoringLogger line to publish.
        :param channel: which channel the data has been measured for. May be 'ALL'. 
        '''

        if line.startswith('#'):
            return
        instrument_name = self._resource[9:-7]
        values = line.split('\t')
        if channel == 'ALL':
            for ch in self.outputs:
                client.publish(f'/{instrument_name}/channel{ch}/voltage', f'{instrument_name} voltage{ch}={float(values[2 * ch - 1].strip())}')
                client.publish(f'/{instrument_name}/channel{ch}/current', f'{instrument_name} current{ch}={float(values[2 * ch].strip())}')
        else:
            self.validate_channel(int(channel))
            client.publish(f'/{instrument_name}/channel{channel}/voltage', f'{instrument_name} voltage{channel}={float(values[1].strip())}')
            client.publish(f'/{instrument_name}/channel{channel}/current', f'{instrument_name} current{channel}={float(values[2].strip())}')

# end class TTI

TTI.generate_methods()

    
