'''
Utility decorators and functions to used with Click library to generate instrument CLIs.

Contains `@with_instrument` and `@print_output` decorators, as well as verbosity conversion function and MonitoringLogger broadcaster class.
'''

import click
import logging
from functools import update_wrapper, wraps

# extend click's pass_context handler to unwrap instrument in with-statement
def with_instrument(function):
    '''
    Wraps decorated function in 'with <instrument>: ...' statement.

    Decorator for CLI function which wraps function in 'with <instrument>: ...' statement to enter and exit instrument context before and after function is executed, respectively.
    
    .. note:: DECORATOR
    '''
    @click.pass_context
    def new_function(ctx, *args, **kwargs):
        with ctx.obj as instrument:
            return ctx.invoke(function, instrument, *args, **kwargs)
    return update_wrapper(new_function, function)

def print_output(function):
    '''
    Decorator for CLI function which prints return value of function after completion.
    
    .. note:: DECORATOR
    '''
    @wraps(function)
    def _print_output(*args, **kwargs):
        ret = function(*args, **kwargs)
        print(ret)
    return _print_output

def verbosity(level):
    '''
    Convert numerical verbosity level to logging level.

    0 => WARNING
    1 => INFO
    2 => DEBUG
    '''
    if level == 0:
        return logging.WARNING
    elif level == 1:
        return logging.INFO
    else:
        return logging.DEBUG

class MonitoringLogger:
    '''
    Multiple output logging interface.

    Multilogger that allows configurable writers (i.e. functions) to be registered, as well as flushers (also functions). On a write to a MonitoringLogger, all registered writers are called, then all flushers.
    '''

    def __init__(self, *writers):
        '''
        Initialise multilogger and immediately register writers if provided.

        :param writers: List of functions to register that should be called by logger on write().
        '''
        for writer in writers:
            assert(callable(writer))
        self.writers = list(writers)
        self.flushers = []

    def register_writer(self, writer):
        '''
        Register writer.

        :param writer: Functions to register that should be called by logger on write().
        '''
        assert(callable(writer))
        self.writers.append(writer)

    def register_flusher(self, flusher):
        '''
        Register flusher.

        :param flusher: Functions to register that should be called by logger after write().
        '''
        assert(callable(flusher))
        self.flushers.append(flusher)

    def write(self, *args, **kwargs):
        '''
        Write to all registered writers, then flush all registered flushers.

        :param \*args: arguments to be passed to writers.
        :param \*kwargs: keyword arguments to be passed to writers.
        '''
        for writer in self.writers:
            writer(*args, **kwargs)
        for flusher in self.flushers:
            flusher()

    def __call__(self, *args, **kwargs):
        '''
        Shorthand for write().
        '''
        self.write(*args, **kwargs)

