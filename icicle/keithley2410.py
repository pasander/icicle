'''
Keithley2410 class for Keithley 2410 source-measure unit.
'''
import argparse
import click
from functools import update_wrapper

from .instrument import Singleton, acquire_lock
from .visa_instrument import retry_on_fail_visa
from .scpi_instrument import SCPIInstrument, is_in, is_integer, is_numeric

class Keithley2410(SCPIInstrument, key='resource'):
    '''
    SCPIInstrument implementation for Keithley2410 source-measure unit.
    '''

    BAUD_RATE = 19200
    ''' Serial link Baud rate. '''
    TIMEOUT = 10000 # 10 seconds
    ''' Serial link timeout (ms). '''

    COM_SELFTEST = '*TST?'
    ''' Selftest SCPI command. '''
    COM_RESET = '*RST; STATUS:PRESET; *CLS'
    ''' Instrument Reset SCPI command. '''
    COM_CLEAR_TRACE = ':TRAC:CLE'
    ''' Clear trace SCPI command. '''
    COM_DEVICE_CLEAR = 'DCL'
    ''' Device Clear SCPI command. '''

    SETTINGS = {
        'IDENTIFIER': {
            'QUERY': '*IDN?'
        },
        'OUTPUT': {
            'SET': ':OUTP {}',
            'QUERY': ':OUTP?',
            'verifier': is_in('ON', 'OFF')
        },
        'PANEL': {
            'SET': ':ROUT:TERM {}',
            'QUERY': ':ROUT:TERM?',
            'verifier': is_in('FRON', 'REAR')
        },
        'SOURCE': {
            'SET': ':SOUR:FUNC {}',
            'QUERY': ':SOUR:FUNC?',
            'verifier': is_in('VOLT', 'CURR')
        },
        'VOLTAGE_RANGE': {
            'SET': ':SOUR:VOLT:RANGE {}',
            'QUERY': ':SOUR:VOLT:RANGE?',
            'verifier': is_integer()
        },
        'VOLTAGE_RANGE_AUTO': {
            'SET': ':SOUR:VOLT:RANGE:AUTO {}',
            'QUERY': ':SOUR:VOLT:RANGE:AUTO?',
            'verifier': is_in('ON', 'OFF')
        },
        'VOLTAGE': {
            'SET': ':SOUR:VOLT:LEV {}',
            'QUERY': ':SOUR:VOLT:LEV?',
            'verifier': is_numeric(min=-10000, max=0.001)
        },
        'DELAY': {
            'SET': ':SOUR:DEL {}',
            'QUERY': ':SOUR:DEL?',
            'verifier': is_numeric()
        },
        'VOLTAGE_MODE': {
            'SET': ':SOUR:VOLT:MODE {}',
            'QUERY': ':SOUR:VOLT:MODE?',
            'verifier': is_in('FIX', 'SWE')
        },
        'VOLTAGE_SWEEP_START': {
            'SET': ':SOUR:VOLT:START {}',
            'QUERY': ':SOUR:VOLT:START?',
            'verifier': is_numeric()
        },
        'VOLTAGE_SWEEP_END': {
            'SET': ':SOUR:VOLT:END {}',
            'QUERY': ':SOUR:VOLT:END?',
            'verifier': is_numeric()
        },
        'VOLTAGE_SWEEP_STEP': {
            'SET': ':SOUR:VOLT:STEP {}',
            'QUERY': ':SOUR:VOLT:STEP?',
            'verifier': is_numeric()
        },
        'SWEEP_DIRECTION': {
            'SET': ':SOUR:SWE:DIR {}',
            'QUERY': ':SOUR:SWE:DIR?',
            'verifier': is_in('UP', 'DOWN')
        },
        'SWEEP_RANGE': {
            'SET': ':SOUR:SWE:RANG {}',
            'QUERY': ':SOUR:SWE:RANG?',
            'verifier': is_in('FIX', 'AUTO', 'BEST')
        },
        'SWEEP_POINTS': {
                'QUERY': ':SOUR:SWE:POIN?'
        },
        'SWEEP_POINTS_SPACE': {
            'SET': ':SOUR:SWE_SPAC {}',
            'QUERY': ':SOUR:SWE:SPAC?',
            'verifier': is_in('LIN', 'LOG')
        },
        'COMPLIANCE_CURRENT': {
            'SET': ':SENS:CURR:PROT {}',
            'QUERY': ':SENS:CURR:PROT?',
            'verifier': is_numeric()
        },
        'SENSE_FUNCTION': {
            'SET': ':SENS:FUNC {}',
            'QUERY': ':SENS:FUNC?',
            'verifier': is_in('"VOLT"', '"CURR"', '"RES"')
        },
        'SENSE_CURRENT_INTEGRATION': {
            'SET': ':SENS:CURR:NPLC {}',
            'QUERY': ':SENS:CURR:NPLC?',
            'verifier': is_numeric(min=0.01, max=100)
        },
        'SENSE_CURRENT_RANGE': {
            'SET': ':SENS:CURR:RANG {}',
            'QUERY': ':SENS:CURR:RANG?',
            'verifier': is_numeric()
        },
        'SENSE_VOLTAGE_INTEGRATION': {
            'SET': ':SENS:VOLT:NPLC {}',
            'QUERY': ':SENS:VOLT:NPLC?',
            'verifier': is_numeric(min=0.01, max=100)
        },
        'SENSE_RESISTANCE_INTEGRATION': {
            'SET': ':SENS:RES:NPLC {}',
            'QUERY': ':SENS:RES:NPLC?',
            'verifier': is_numeric(min=0.01, max=100)
        },
        'SENSE_AVERAGING': {
            'SET': ':SENS:AVER {}',
            'QUERY': ':SENS:AVER?',
            'verifier': is_in('ON', 'OFF')
        },
        'SENSE_AVERAGING_COUNT': {
            'SET': ':SENS:AVER:COUN {}',
            'QUERY': ':SENS:AVER:COUN?',
            'verifier': is_integer(min=1, max=100)
        },
        'SENSE_AVERAGING_TYPE': {
            'SET': ':SENS:VOLT:NPLC {}',
            'QUERY': ':SENS:AVER:TCON?',
            'verifier': is_in('MOV', 'REP')
        },
        'TRIGGER_COUNT': {
            'SET': ':TRIG:COUN {}',
            'QUERY': ':TRIG:COUN?',
            'verifier': is_integer()
        },
        'AUTOZERO': {
            'SET': ':SYST:AZER:STAT {}',
            'QUERY': ':SYS:AZER:STAT?',
            'verifier': is_in('ON', 'OFF', 'ONCE')
        },
        'READ': {
            'QUERY': ':READ?'
        },
        'MEASURE': {
            'QUERY': ':MEAS?'
        }
    }
    ''' Settings dictionary with all Set/Query SCPI combinations. '''


    def __init__(self, resource = 'ASRL/dev/ttyUSB1::INSTR', reset_on_init = True, ramp_down_on_close=False):
        '''
        .. Warning: the ``resource`` keyword argument is mandatory and must be explicitly specified - failing to do so will result in an error since the Multiton metaclass on VisaInstrument masks this default value for ``resource``.

        :param resource: VISA Resource address. See VISA docs for more info.
        :param reset_on_init: Whether to reset Keithley2410 using COM_RESET command on device initialisation.
        :param ramp_down_on_close: Whether to ramp down output with default ramp parameters on instrument close.
        '''
        super().__init__(resource, reset_on_init)
        self._ramp_down_on_close = ramp_down_on_close

    @acquire_lock()
    def __enter__(self, recover_attempt = False):
        '''
        Initialises connection to Keithley 2410.

        :param recovery_attempt: Whether this is a recovery attempt by retry_on_fail.

        :return: `Keithley2410` object in activated state.
        '''
        super().__enter__(recover_attempt=recover_attempt, no_lock = True)
        if self._reset_on_init and not recover_attempt:
            if self.status() != 0:
                raise RuntimeError('Output on for Keithley2410, but reset_on_init set. Dangerous - refusing to connect.')
            self._selftest = self.selftest(no_lock = True)
            # Set default values here
            self.set('VOLTAGE_MODE', 'FIX', no_lock=True)
            self.set('VOLTAGE_MODE', 'FIX', no_lock=True)
            self.set('VOLTAGE', 0, no_lock=True)
            self.set('VOLTAGE_RANGE', 1000, no_lock=True)
            self.set('PANEL', 'FRON')
            # Set up by default to measure current
            self.set('SENSE_FUNCTION', 'CURR')
            self.set('AUTOZERO', 'ON')
            self.set('COMPLIANCE_CURRENT', 1e-6)
        return self

    @acquire_lock()
    def __exit__(self, exception_type = None, exception_value = None, traceback = None, recover_attempt = False):
        '''
        Closes connection to Keithley 2000.

        :param recovery_attempt: Whether this is a recovery attempt by retry_on_fail.

        :return: `HMP4040` object in activated state.
        '''
        if self._ramp_down_on_close and not recover_attempt:
            # Should be safe for pretty much anything
            self.sweep('VOLTAGE', 0, delay=1, step_size=5, no_lock=True)
            self.off(no_lock=True)
        super().__exit__(exception_type, exception_value, traceback, no_lock=True)

    @acquire_lock()
    def selftest(self):
        '''
        Run self-test.
        
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).

        :returns: result of `COM_SELFTEST` SCPI command.
        '''
        return self._instrument.query(type(self).COM_SELFTEST)
    
    @acquire_lock()
    def device_clear(self):
        '''
        Send Device Clear command.
        
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).

        :returns: result of `COM_DCL` SCPI command.
        '''
        return self._instrument.query(type(self).COM_DEVICE_CLEAR)
    
    def off(self, **kwargs):
        '''
        Turn output off.
        
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :returns: result of set(...).
        '''
        # uses lock from sub-call
        return self.set('OUTPUT', 'OFF', **kwargs)
    
    def on(self, **kwargs):
        '''
        Turn output on.
        
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :returns: result of set(...).
        '''
        # uses lock from sub-call
        return self.set('OUTPUT', 'ON', **kwargs)

    def status(self, **kwargs):
        '''
        Read output status.
        
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :returns: result of query(...).
        '''
        # uses lock from sub-call
        return int(self.query('OUTPUT', **kwargs))
    
    @acquire_lock()
    @retry_on_fail_visa()
    def clear_trace(self):
        '''
        Clears measurement trace.
        
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :returns: result of `COM_CLEAR_TRACE` SCPI command.
        '''
        return self._instrument.write(type(self).COM_CLEAR_TRACE)
    
    @acquire_lock()
    def measure(self, repetitions=1, delay=None, clear_trace=True):
        '''
        Measure output voltage, current averaged over `repetitions` measurements. 

        :param repetitions: how many measurements to make. Defaults to 1.
        :param delay: delay between measurements. Only used when repetitions > 1.
        :param clear_trace: whether to clear trace after measurements. Defaults to True.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).

        :returns: tuple of `(volts, amps, ohms, timestamp, filter)`, averaged over measurements.
        '''
        self.set('TRIGGER_COUNT', repetitions, no_lock=True)
        self.set('SENSE_FUNCTION', '"VOLT"', no_lock=True)
        self.set('SENSE_FUNCTION', '"CURR"', no_lock=True)
        if delay is not None:
            self.set('DELAY', delay, no_lock=True)
        volts, amps, ohms, timestamp, filter_ = [float(val) for val in self.query('READ', no_lock=True).split(',')]
        if clear_trace:
            self.clear_trace(no_lock=True)
        return volts, amps, ohms, timestamp, filter_

    def measure_single(self, clear_trace=True, **kwargs):
        '''
        Perform single measurement.

        :param clear_trace: whether to clear trace after measurements. Defaults to True.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).

        :returns: tuple of `(volts, amps, ohms, timestamp, filter)`.
        '''
        # uses lock from sub-call
        return self.measure(repetitions=1, delay=None, clear_trace=clear_trace, **kwargs)

    def publish(self, client, line):
        '''
        Publish data line (from Monitoring) to MQTT.

        TODO: don't reparse line here; instead pass/access raw data? 

        :param client: MQTTClient object.
        :param line: logfile line from MonitoringLogger.
        '''
        if line.startswith('#'):
            return
        instrument_name = self._resource[9:-7]
        values = line.split('\t')
        client.publish(f'/{instrument_name}/voltage', f'{instrument_name} voltage={float(values[1].strip())}')
        client.publish(f'/{instrument_name}/current', f'{instrument_name} current={float(values[2].strip())}')
        client.publish(f'/{instrument_name}/resistance', f'{instrument_name} resistance={float(values[3].strip())}')
        client.publish(f'/{instrument_name}/PSUTime', f'{instrument_name} PSUTime={float(values[4].strip())}')
        client.publish(f'/{instrument_name}/statusRegister', f'{instrument_name} statusRegister={float(values[5].strip())}')

# end class Keithley2410


Keithley2410.generate_methods()

    
