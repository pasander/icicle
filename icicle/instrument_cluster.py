'''
InstrumentCluster class wrapping HV, LV, Relay Board and Multimeter.

This module also contains the `DummyInstrument`, `DummyFunction` and `InstrumentNotInstantiated` classes, and `MissingRequiredInstrumentError` and `BadStatusForOperationError` exceptions.
'''

from .instrument import Instrument, acquire_lock, retry_on_fail
import logging
import time

logger = logging.getLogger(__name__)

class InstrumentNotInstantiated:
    '''
    Filler class to replace response from a missing instrument in the cluster. 
    '''
    def __str__(self):
        return 'InstrumentNotInstantiated'
    
    def __repr__(self):
        return str(self)

class DummyInstrument:
    '''
    Filler class to replace an instrument in the cluster that has not been provided or instantiated.

    All calls to any possible function should return an InstrumentNotInstantiated object, except init, enter, exit...
    '''

    class DummyFunction(InstrumentNotInstantiated):
        def __init__(self, instrument, name):
            self._instrument = instrument
            self._name = name

        def __call__(self, *args, **kwargs):
            logger.warn(f'Instrument "{self._instrument}" not provided - command {self._name}({", ".join([str(a) for a in args] + [str(key) + ": " + str(value) for key, value in kwargs.items()])}) ignored.')
            return InstrumentNotInstantiated()

        def __str__(self):
            return f'Instrument "{self._instrument}" not provided - value {self._name} returned as DummyFunction.'

        def __repr__(self):
            return str(self)

    def __init__(self, instrument):
        self._instrument = instrument

    def __getattr__(self, name):
        return type(self).DummyFunction(self._instrument, name)

    def __enter__(self, recover_attempt = False):
        return self

    def __exit__(self, exception_type = None, exception_value = None, traceback = None, recover_attempt = False):
        pass
       
class MissingRequiredInstrumentError(RuntimeError):
    '''
    Error to be thrown if an instrument required for the cluster is missing.
    '''
    pass

class BadStatusForOperationError(RuntimeError):
    '''
    Error to be thrown if an instrument has incorrect status for the requested operation.
    '''
    pass

class InstrumentCluster(Instrument):
    '''
    Instrument cluster class designed to provide seamless access to a cluster consisting of HV, LV, Relay Board and Multimeter, with required protections to ensure these are only used in safe configurations.
    '''

    # Specify which package to use for which instrument class. 
    package_map = {
            # HV
            'Keithley2410': 'keithley2410',
            # LV
            'TTI': 'tti',
            'HMP4040': 'hmp4040',
            # Relay Board
            'RelayBoard': 'relay_board',
            # Multimeter
            'Keithley2000': 'keithley2000',
        }

    def __init__(self, 
                 lv = None, hv = None, relay_board = None, multimeter = None, 
                 lv_resource = 'ASRL/dev/ttyUSB3::INSTR',
                 hv_resource = 'ASRL/dev/ttyACM0::INSTR',
                 relay_board_resource = 'ASRL/dev/ttyUSB1::INSTR',
                 multimeter_resource = 'ASRL/dev/ttyUSB2::INSTR',
                 reset_on_init = False, turn_off_on_close = True,
                 default_lv_channel = 1, default_lv_voltage = 1.8, default_lv_current = 6.0, 
                 default_hv_voltage = -60.0, default_hv_compliance_current = None,
                 default_hv_delay=1.0, default_hv_step_size=5.0):
        '''
        Instantiate an instrument cluster by references to LV, HV, Relay Board, and Multimeter, or subset of these. Set default values for some settings.

        :param lv: Class to be used for LV instrument (TTI or HMP4040 or None).
        :param hv: Class to be used for HV instrument (Keithley2410 or None).
        :param relay_board: Class to be used for Relay Board instrument (RelayBoard or None).
        :param multimeter: Class to be used for Multimeter instrument (Keithley2000 or None).
        :param lv_resource: VISA Resource address for LV.
        :param hv_resource: VISA Resource address for HV.
        :param relay_board_resource: VISA Resource address for Relay Board.
        :param multimeter_resource: VISA Resource address for Multimeter.
        :param reset_on_init: Whether to reset all devices on (first) __enter__ call.
        :param turn_off_on_close: Whether to turn off instrument cluster on last __exit__ call.
        :param default_lv_channel: Default channel to use for LV supply.
        :param default_lv_voltage: Default voltage on LV for all operations.
        :param default_lv_current: Default current on LV for all operations.
        :param default_hv_voltage: Default voltage on HV for all operations.
        :param default_hv_compliance_current: Default compliance current to be set on HV.
        :param default_hv_delay: Default delay during HV ramps.
        :param default_hv_step_size: Default step size for HV ramps.
        '''
        super().__init__()

        self._default_lv_channel=default_lv_channel
        self._default_lv_voltage=default_lv_voltage
        self._default_lv_current=default_lv_current
        self._default_hv_voltage=default_hv_voltage
        self._default_hv_compliance_current=default_hv_compliance_current
        self._default_hv_delay=default_hv_delay
        self._default_hv_step_size=default_hv_step_size

        if lv is None and hv is not None:
            raise MissingRequiredInstrumentError('Cannot instantiate InstrumentCluster with "hv" and without "lv" instrument. Safe control hierarchy requires "lv" as prerequisite for "hv".')
        if lv is None and multimeter is not None:
            raise MissingRequiredInstrumentError('Cannot instantiate InstrumentCluster with "multimeter" and without "lv" instrument. Safe control hierarchy requires "lv" as prerequisite for "multimeter". Also what are you measuring without power?')

        self._lv = (
            Instrument.load_instrument_class(InstrumentCluster.package_map[lv], lv)(
                resource=lv_resource, reset_on_init=reset_on_init, off_on_close=turn_off_on_close
            ) if lv is not None else DummyInstrument('lv')
        )
        self._hv = (
            Instrument.load_instrument_class(InstrumentCluster.package_map[hv], hv)(
                resource=hv_resource, reset_on_init=reset_on_init, ramp_down_on_close=turn_off_on_close
            ) if hv is not None else DummyInstrument('hv')
        )
        self._relay_board = (
            Instrument.load_instrument_class(InstrumentCluster.package_map[relay_board], relay_board)(
                resource=relay_board_resource, disconnect_on_exit=turn_off_on_close
            ) if relay_board is not None else DummyInstrument('relay_board')
        )
        self._multimeter = (
            Instrument.load_instrument_class(InstrumentCluster.package_map[multimeter], multimeter)( 
                resource=multimeter_resource, reset_on_init=reset_on_init
            ) if multimeter is not None else DummyInstrument('multimeter')
        )

    def sleep(self, delay):
        '''
        Utility function to allow sleep to return True.

        :param delay: Sleep time in seconds.
        '''
        time.sleep(delay)
        return True

    def assert_status(self, current_status, required_status, message):
        '''
        Assert that current_status is required_status or current_status is of type InstrumentNotInstantiated, else raise exception and shut down InstrumentCluster.

        :param current_status: Result of query or similar on current instruments.
        :param required_status: Expected (i.e. valid) result.
        :param message: Exception message in case of failure.
        '''
        if isinstance(current_status, InstrumentNotInstantiated):
            return
        if current_status != required_status:
            input(f'Bad status! {message} Press enter to continue: ')
            raise BadStatusForOperationError(message)

    @acquire_lock()
    def __enter__(self, recover_attempt = False):
        '''
        Begin context. Enters context on LV, HV, Relay Board, Multimeter in order, hence initialising these instruments. Sets HV compliance current immediately on startup.
        '''
        self._lv.__enter__(recover_attempt=recover_attempt)
        self._hv.__enter__(recover_attempt=recover_attempt)
        self._relay_board.__enter__(recover_attempt=recover_attempt)
        self._multimeter.__enter__(recover_attempt=recover_attempt)
        super().__enter__(recover_attempt=recover_attempt, no_lock=True)

        if self._default_hv_compliance_current is not None:
            self.hv_compliance_current(self._default_hv_compliance_current, no_lock=True)
        return self

    @acquire_lock()
    def __exit__(self, exception_type = None, exception_value = None, traceback = None, recover_attempt = False):
        '''
        Ends context. Leaves context on Multimeter, Relay Board, HV, LV in order, hence closing these instruments.

        :param exception_type: Exception type thrown causing close, or None.
        :param exception_value: Exception thrown causing close, or None.
        :param traceback: Exception traceback.
        :param recover_attempt: Whether this is a recovery attempt when instrument fails/disconnects (i.e. from @retry_on_fail).
        '''
        self._multimeter.__exit__(recover_attempt=recover_attempt)
        self._relay_board.__exit__(recover_attempt=recover_attempt)
        self._hv.__exit__(recover_attempt=recover_attempt)
        self._lv.__exit__(recover_attempt=recover_attempt)
        super().__exit__(recover_attempt=recover_attempt, no_lock=True)
    
    def reset(self, *args, **kwargs):
        '''
        Resets all instruments.

        :param no_lock: Do not acquire lock for subinstruments. (@acquire_lock)
        :param attempts: Retry attempts for each subinstrument. (@retry_on_fail)
        '''
        self.assert_status(self._lv.status(self._default_lv_channel) == 0 and self._hv.status() == 0, '"lv" or "hv" still on - will not reset cluster')
        return (self._lv.reset(*args, **kwargs)
                and self._hv.reset(*args, **kwargs)
                and self.hv_compliance_current(default_hv_compliance_current)
                and self._relay_board.reset(*args, **kwargs)
                and self._multimeter.reset(*args, **kwargs))

    @acquire_lock()
    def relay_pin(self, pin = 'OFF', lv_channel = None):
        '''
        Set relay pin.

        Requires LV to be off, and Multimeter to not be in resistance sensing mode unless changing pin to/from NTC.

        For valid pins, see list in the corresponding RelayBoard class.

        :param pin: Pin, see list/dict in the corresponding RelayBoard class.
        :param lv_channel: Which LV channel is in use. Defaults to default_lv_channel as specified on initialisation if None.

        :param no_lock: Do not attempt to acquire lock. (@acquire_lock)
        '''
        if lv_channel is None:
            lv_channel = self._default_lv_channel

        current_pin = self._relay_board.query_pin()
        if current_pin == pin:
            return True
        
        self.assert_status(self._lv.status(lv_channel), 0, '"lv" is on - refusing to change relay_board pin for safety')
        sense_function = self._multimeter.query('SENSE_FUNCTION')
        if not isinstance(sense_function, InstrumentNotInstantiated) and 'RES' in sense_function:
            self.assert_status(pin in ('OFF', 'NTC'), True, 'Multimeter is on Resistance sensing mode - refusing to change relay_board pin to anything other than OFF or NTC')

        return self._relay_board.set_pin(pin)

    @acquire_lock()
    def multimeter_measure(self, what, repetitions=1, delay=None, cycles=1.0, clear_trace=True):
        '''
        Measure using multimeter

        Requires relay board to be connecting OFF or NTC if `what` is RES/FRES.

        :param what: Requested measurement type. Accepted are `VOLT:DC`, `VOLT:AC`, `CURR:DC`, `CURR:AC`, `RES`, `FRES`.
        :param repetitions: How many measurements to average. Defaults to 1.
        :param delay: Delay between repetitions (seconds).
        :param cycles: How many integration cycles per measurement. Defaults to 1.0.
        :param clear_trace: Whether to send 'clear trace' command after measurement. Default to True.
        
        :param no_lock: Do not attempt to acquire lock. (@acquire_lock)
        '''
        assert(what in ('VOLT:DC', 'VOLT:AC', 'CURR:DC', 'CURR:AC', 'RES', 'FRES'))
        if what in ('RES', 'FRES'):
            self.assert_pin_in(self._relay_board.query_pin(), ('OFF', 'NTC'), 'Relay board has pin other than OFF or NTC connected - Multimeter refusing to enter Resistance sensing mode')
        return self._multimeter.measure(what, repetitions=repetitions, delay=delay, cycles=cycles, clear_trace=clear_trace)

    @acquire_lock()
    def lv_on(self, lv_channel = None, voltage = None, current = None):
        '''
        Turn on LV.

        Requires HV to be off.

        :param lv_channel: Which LV channel is in use. Defaults to default_lv_channel as specified on initialisation if None.
        :param voltage: Voltage to set on LV. Defaults on default_hv_voltage as specified on initialisation if None.
        :param current: Current to set on LV. Defaults on default_hv_current as specified on initialisation if None.

        :param no_lock: Do not attempt to acquire lock. (@acquire_lock)
        '''
        if voltage is None: 
            voltage = self._default_lv_voltage
        if current is None: 
            current = self._default_lv_current
        if lv_channel is None:
            lv_channel = self._default_lv_channel
        #self.assert_status(self._lv.status(lv_channel) == 0, '"lv" already on - cowardly refusing to start "lv" or change voltage')
        self.assert_status(self._hv.status(), 0, '"hv" on but "lv" off - DANGEROUS STATE! - Refusing to start "lv"')
        time.sleep(0.5)
        self._lv.set_channel('VOLTAGE', lv_channel, voltage)
        time.sleep(0.5)
        self._lv.set_channel('CURRENT', lv_channel, current)
        time.sleep(0.5)
        return self._lv.on(lv_channel)
    
    @acquire_lock()
    def lv_off(self, lv_channel = None):
        '''
        Turn off LV.

        Requires HV to be off.

        :param lv_channel: Which LV channel is in use. Defaults to default_lv_channel as specified on initialisation if None.

        :param no_lock: Do not attempt to acquire lock. (@acquire_lock)
        '''
        if lv_channel is None:
            lv_channel = self._default_lv_channel
        if self._lv.status(lv_channel) == 0:
            # Already off
            return True
        self.assert_status(self._hv.status(), 0, '"hv" is ON - DANGEROUS STATE! - Refusing to turn off "lv"')
        return self._lv.off(lv_channel)
    
    @acquire_lock()
    def lv_sweep(self, what, target, delay, step_size, lv_channel=None, measure=False):
        '''
        Sweep/Ramp LV from current to target voltage.

        Requires HV to be off.

        :param what: name of setting that `set()` call should target.
        :param target: value of setting to sweep to (from current).
        :param delay: delay between sweep steps (in seconds)
        :param step_size: step size between sweep steps
        :param lv_channel: Which LV channel is in use. Defaults to default_lv_channel as specified on initialisation if None.
        :param measure: whether to measure at each step of sweep.

        :param no_lock: Do not attempt to acquire lock. (@acquire_lock)
        '''
        if lv_channel is None: 
            lv_channel = self._default_lv_channel
        self.assert_status(self._hv.status(), 0, '"hv" is on - refusing to ramp "lv" whilst "hv" powered.')
        return ((self._lv.on() if self._lv.status() == 0 else True)
                and (ret := self._lv.sweep(what, target, delay, step_size, measure=measure, log_function=logger.info))
                and (self._lv.off() if abs(target) < 1e-12 else True)
                and ret)
    
    @acquire_lock()
    def hv_on(self, lv_channel = None, voltage = None, delay=None, step_size=None, measure=False):
        '''
        Turn on HV and ramps to voltage.

        Requires LV to be on.

        :param lv_channel: Which LV channel is in use. Defaults to default_lv_channel as specified on initialisation if None.
        :param voltage: Voltage to set on HV. Defaults on default_hv_voltage as specified on initialisation if None.
        :param delay: delay between sweep steps (in seconds)
        :param step_size: step size between sweep steps
        :param measure: whether to measure at each step of sweep.

        :param no_lock: Do not attempt to acquire lock. (@acquire_lock)
        '''
        if lv_channel is None: 
            lv_channel = self._default_lv_channel
        if voltage is None: 
            voltage = self._default_hv_voltage
        if delay is None:
            delay = self._default_hv_delay
        if step_size is None:
            step_size = self._default_hv_step_size
        #self.assert_status(self._hv.status() == 0, '"hv" already on - cowardly refusing to start "hv" or change voltage')
        for i in range(10):
            print(self._lv.status(lv_channel))
        self.assert_status(self._lv.status(lv_channel), 1, '"lv" is OFF - refusing to start "hv"')
        return (self._hv.set('VOLTAGE', 0.0)
                and self._hv.on()
                and self._hv.sweep(target_setting='VOLTAGE', target_value=voltage, delay=delay, step_size=step_size, measure=measure, log_function=logger.info))
    
    @acquire_lock()
    def hv_off(self, lv_channel=None, delay=None, step_size=None, measure=False):
        '''
        Ramps HV to zero and turns off.

        Requires LV to be on.

        :param lv_channel: Which LV channel is in use. Defaults to default_lv_channel as specified on initialisation if None.
        :param delay: delay between sweep steps (in seconds)
        :param step_size: step size between sweep steps
        :param measure: whether to measure at each step of sweep.

        :param no_lock: Do not attempt to acquire lock. (@acquire_lock)
        '''
        if self._hv.status() == 0:
            # Already off
            return True
        if lv_channel is None:
            lv_channel = self._default_lv_channel
        if delay is None:
            delay = self._default_hv_delay
        if step_size is None:
            step_size = self._default_hv_step_size
        self.assert_status(self._lv.status(lv_channel), 1, '"lv" is OFF but "hv" requested ramp down - refusing to touch "hv". Please fix manually.')
        if self._hv.status() == 0:
            # Already off
            return True
        return ((ret := self._hv.sweep(target_setting='VOLTAGE', target_value=0.0, delay=delay, step_size=step_size, measure=measure, log_function=logger.info) is not False)
                and self._hv.off() is not False
                and ret)

    @acquire_lock()
    def hv_compliance_current(self, compliance_current):
        ''' 
        Set HV compliance current and corresponding sense current range.

        :param compliance_current: Compliance in Amps.
        '''
        return (self._hv.set('COMPLIANCE_CURRENT', compliance_current) and 
                self._hv.set('SENSE_CURRENT_RANGE', compliance_current))

    @acquire_lock()
    def on(self, relay_pin='OFF',
           lv_channel=None, lv_voltage=None, lv_current=None, 
           hv_voltage=None, hv_delay=None, hv_step_size=None,
           measure=False):
        '''
        Turn all instruments on (and ramp up) in correct order and with sensible delays.

        :param relay_pin: Pin, see list/dict in the corresponding RelayBoard class.
        :param lv_channel: Which LV channel is in use. Defaults to default_lv_channel as specified on initialisation if None.
        :param lv_voltage: Voltage to set on LV. Defaults on default_hv_voltage as specified on initialisation if None.
        :param lv_current: Current to set on LV. Defaults on default_hv_current as specified on initialisation if None.
        :param hv_voltage: Voltage to ramp to on HV. Defaults on default_hv_voltage as specified on initialisation if None.
        :param hv_delay: delay between HV sweep steps (in seconds)
        :param hv_step_size: step size between HV sweep steps
        :param measure: whether to measure at each step of HV sweep.

        :param no_lock: Do not attempt to acquire lock. (@acquire_lock)
        '''
        if lv_channel is None: 
            lv_channel = self._default_lv_channel
        if lv_voltage is None: 
            lv_voltage = self._default_lv_voltage
        if lv_current is None: 
            lv_current = self._default_lv_current
        if hv_voltage is None: 
            hv_voltage = self._default_hv_voltage
        if hv_delay is None:
            hv_delay = self._default_hv_delay
        if hv_step_size is None:
            hv_step_size = self._default_hv_step_size
        return (self.relay_pin(relay_pin, no_lock=True) is not False
                and self.sleep(1) 
                and self.lv_on(lv_channel=lv_channel, voltage=lv_voltage, current=lv_current, no_lock=True) is not False
                and self.sleep(2)
                and self.hv_on(voltage=hv_voltage, delay=hv_delay, step_size=hv_step_size, measure=measure, no_lock=True))

    @acquire_lock()
    def off(self, 
           lv_channel=None, 
           hv_delay=None, hv_step_size=None,
           measure=False):
        '''
        Turn all instruments off (and ramp down) in correct order.

        :param relay_pin: Pin, see list/dict in the corresponding RelayBoard class.
        :param lv_channel: Which LV channel is in use. Defaults to default_lv_channel as specified on initialisation if None.
        :param hv_delay: delay between HV sweep steps (in seconds)
        :param hv_step_size: step size between HV sweep steps
        :param measure: whether to measure at each step of HV sweep.

        :param no_lock: Do not attempt to acquire lock. (@acquire_lock)
        '''
        if lv_channel is None: 
            lv_channel = self._default_lv_channel
        if hv_delay is None:
            hv_delay = self._default_hv_delay
        if hv_step_size is None:
            hv_step_size = self._default_hv_step_size
        return ((ret := self.hv_off(delay=hv_delay, step_size=hv_step_size, measure=measure, no_lock=True)) is not False
                and self.sleep(1)
                and self.lv_off(lv_channel=lv_channel, no_lock=True) is not False
                and self.sleep(2)
                and self.relay_pin('OFF', no_lock=True) is not False
                and ret)

    @acquire_lock()
    def status(self, lv_channel, *args, **kwargs):
        '''
        Request output/pin status for HV, LV and Relay board. Returns all results as dictionary.

        :param lv_channel: Which LV channel is in use. Defaults to default_lv_channel as specified on initialisation if None.
        '''
        if lv_channel is None:
            lv_channel = self._default_lv_channel
        return {
                'hv': self._hv.status(), 
                'lv': self._lv.status(lv_channel),
                'relay_board': self._relay_board.status()
        }

    def abort(self, *args, **kwargs):
        '''
        Alias for off().
        '''
        self.off(*args, **kwargs)

    def open(self, *args, **kwargs):
        '''
        Alias for __enter__().
        '''
        self.__enter__(*args, **kwargs)

    def close(self, *args, **kwargs):
        '''
        Alias for __exit__().
        '''
        self.__exit__(*args, **kwargs)

# end class InstrumentCluster
