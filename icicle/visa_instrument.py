'''
pyVISA Instrument interface and associated decorator tools. 

This module contains the `VISAInstrument` interface and the `@retry_on_fail_visa` decorator.
'''

from .instrument import Instrument, Multiton, acquire_lock, retry_on_fail
import pyvisa

constants = pyvisa.constants
       
def retry_on_fail_visa(attempts = 3, exceptions=[pyvisa.VisaIOError]):
    '''
    Specific implementation of `retry_on_fail` for visa instruments, which retries on `VisaIOErrors` by default.

    :returns: Decorated function.
    '''
    return retry_on_fail(attempts, exceptions)
# end def retry_on_fail_visa
        

class VisaInstrument(Instrument, metaclass=Multiton, key='resource'):
    '''
    Implementation of the Instrument Class with instantiation calls to the `pyvisa` package to instantiate a VISA-compatible GPIB, Serial, Ethernet, ... instrument.

    .. Warning: All subclasses of VISAInstrument must have the 'key' keyword class argument defined to specify which '__init__' keyword argument should be used as the Multiton key.
    '''
    
    def __init__(self, resource=''):
        '''
        .. Note: All arguments must be provided as explicit keyword argument 

        :param resource: VISA resource string. See pyvisa docs for examples.
        '''
        self._resource = resource
        super().__init__()

    @acquire_lock()
    def __enter__(self, recover_attempt = False):
        '''
        Connects to instrument using pyvisa, saves raw VISA handle as _instrument. Attempts to find parameters BAUD_RATE and TIMEOUT on the current class.

        :param recovery_attempt: Whether this is a recovery attempt by `retry_on_fail`. Should be propagated.

        :returns: Instrument object with activated context.
        '''
        if not self._connected:
            #if hasattr(type(self), 'BAUD_RATE'):
            #    self._instrument = pyvisa.ResourceManager().open_resource(self._resource, baud_rate=type(self).BAUD_RATE)
            #else:
            self._instrument = pyvisa.ResourceManager().open_resource(self._resource)
            if hasattr(type(self), 'TIMEOUT'):
                self._instrument.timeout = type(self).TIMEOUT
            if hasattr(type(self), 'READ_TERMINATION'):
                self._instrument.read_termination = type(self).READ_TERMINATION
            if hasattr(type(self), 'WRITE_TERMINATION'):
                self._instrument.write_termination = type(self).WRITE_TERMINATION
            # Serial instruments only
            if hasattr(type(self), 'BAUD_RATE'):
                self._instrument.baud_rate = type(self).BAUD_RATE
            if hasattr(type(self), 'DATA_BITS'):
                self._instrument.data_bits = type(self).DATA_BITS
            if hasattr(type(self), 'STOP_BITS'):
                self._instrument.stop_bits = type(self).STOP_BITS
            if hasattr(type(self), 'PARITY'):
                self._instrument.parity = type(self).PARITY
        return super().__enter__(no_lock = True, recover_attempt=recover_attempt)

    @acquire_lock()
    def __exit__(self, exception_type = None, exception_value = None, traceback = None, recover_attempt = False):
        '''
        Closes connection handle to instrument.

        :param recovery_attempt: Whether this is a recovery attempt by `retry_on_fail`. Should be propagated.
        '''

        if self._connected:
            self._instrument.close()
        super().__exit__(exception_type, exception_value, traceback, no_lock = True, recover_attempt=recover_attempt)

# end class VisaInstrument
        
        
