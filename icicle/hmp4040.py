'''
HMP4040 class for Rhode&Schwarz/Hameg HMP4040 4-output power supply. 
'''
import argparse
import click
from functools import update_wrapper

from .instrument import acquire_lock
from .visa_instrument import retry_on_fail_visa
from .scpi_instrument import SCPIInstrument, is_in, is_integer, is_numeric, truthy

class HMP4040(SCPIInstrument, key='resource'):
    '''
    SCPIInstrument implementation for Rhode&Schwarz/Hameg HMP4040 4-output power supply. 

    Has some strange behaviours - take care to note difference between activating individual outputs and "turning on" all outputs.
    '''

    TIMEOUT = 10000 # 10 seconds
    ''' Ethernet link timeout. '''

    COM_RESET = '*RST; STATUS:PRESET; *CLS'
    ''' Instrument Reset SCPI command. '''
    COM_OVP_CLEAR = 'VOLT:PROT:CLE'
    ''' Over-Voltage Protection Clear SCPI command. '''

    SETTINGS = {
        'INSTRUMENT': {
            'SET': 'INST:NSEL {}',
            'QUERY': 'INST:NSEL?',
            'verifier': is_in(1, 2, 3, 4, '1', '2', '3', '4'),
        },
        'IDENTIFIER': {
            'QUERY': '*IDN?'
        },
        'OUTPUT': {
            'SET': 'OUTP {}',
            'QUERY': 'OUTP?',
            'verifier': truthy(true_output=1, false_output=0)
        },
        'OUTPUT_GENERAL': {
            'SET': 'OUTP:GEN {}',
            'QUERY': 'OUTP:GEN?',
            'verifier': truthy(true_output=1, false_output=0)
        },
        'OUTPUT_SELECT': {
            'SET': 'OUTP:SEL {}',
            'QUERY': 'OUTP:SEL?',
            'verifier': truthy(true_output=1, false_output=0)
        },
        'OVP': {
            'SET': 'VOLT:PROT {}',
            'QUERY': 'VOLT:PROT?',
            'verifier': is_numeric()
        },
        'OVP_TRIPPED': {
            'QUERY': 'VOLT:PROT:TRIP?'
        },
        'OUTPUT_CURRENT': {
            'QUERY': 'MEAS:CURR?'
        },
        'OUTPUT_VOLTAGE': {
            'QUERY': 'MEAS:VOLT?'
        },
        'VOLTAGE': {
            'SET': 'VOLT {}',
            'QUERY': 'VOLT?',
            'verifier': is_numeric()
        },
        'CURRENT': {
            'SET': 'CURR {}',
            'QUERY': 'CURR?',
            'verifier': is_numeric()
        }    
    }
    ''' Settings dictionary with all Set/Query SCPI combinations. '''


    def __init__(self, resource = 'TCPIP0::192.168.4.120::5025::SOCKET', reset_on_init = True, off_on_close=False):
        '''
        .. Warning: the ``resource`` keyword argument is mandatory and must be explicitly specified - failing to do so will result in an error since the Multiton metaclass on VisaInstrument masks this default value for ``resource``.

        :param resource: VISA Resource address. See VISA docs for more info.
        :param reset_on_init: Whether to reset HMP4040 using COM_RESET command on device initialisation.
        :param off_on_close: Whether to turn off outputs on __exit__.
        '''
        super().__init__(resource, reset_on_init)
        self._off_on_close = off_on_close

    @acquire_lock()
    def __enter__(self, recover_attempt = False):
        '''
        Initialises connection to HMP4040.

        :param recovery_attempt: Whether this is a recovery attempt by retry_on_fail.

        :return: `HMP4040` object in activated state.
        '''
        super().__enter__(recover_attempt=recover_attempt, no_lock = True)
        if self._reset_on_init and not recover_attempt:
            # Set default values here
            pass
        return self

    @acquire_lock()
    def __exit__(self, exception_type = None, exception_value = None, traceback = None, recover_attempt = False):
        '''
        Closes connection to HMP4040.

        :param recovery_attempt: Whether this is a recovery attempt by retry_on_fail.
        '''
        if self._off_on_close and not recover_attempt:
            # Should be safe for pretty much anything
            for output in (1, 2, 3, 4):
                self.off(output, no_lock=True)
        super().__exit__(exception_type, exception_value, traceback, no_lock=True)

    @acquire_lock()
    def set_channel(self, setting, channel, value):
        '''
        Set `setting` on instrument to `value` for output `channel`, and read-back using equivalent `query()`.

        .. warning: Modifies currently selected `INSTRUMENT` on HMP4040 interface - this change persists.

        :param setting: key in class dictionary SETTINGS.
        :param channel: channel to set this setting on.
        :param value: target value for `setting`.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :return: read-back value if `query()` available, else whether number of bytes written during `set()` meets expectation
        '''
        if not self.set('INSTRUMENT', channel, no_lock=True):
            raise RuntimeError('Could not select correct output {channel}')
        return self.set(f'{setting}', value, no_lock=True)

    @acquire_lock()
    def query_channel(self, setting, channel, **kwargs):
        '''
        Query `setting` on instrument for output `channel`.

        .. warning: Modifies currently selected `INSTRUMENT` on HMP4040 interface - this change persists.

        :param setting: key in class dictionary SETTINGS.
        :param channel: output to set this setting on.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :return: data returned by device for given query.
        '''
        if not self.set('INSTRUMENT', channel, no_lock=True):
            raise RuntimeError('Could not select correct output {channel}')
        return self.query(f'{setting}', no_lock=True)

    def off(self, channel, **kwargs):
        '''
        Turn off output.

        :param channel: output to be turned off.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.
        
        :return: read-back value.
        '''
        # uses lock from sub-call
        return self.set_channel('OUTPUT', channel, 0, **kwargs)
    
    def on(self, channel, **kwargs):
        '''
        Turn on output.

        :param channel: output to be turned on.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.
        
        :return: read-back value.
        '''
        # uses lock from sub-call
        return self.set_channel('OUTPUT', channel, 1, **kwargs)

    def status(self, channel, **kwargs):
        '''
        Check status of output.

        :param channel: output to be turned on.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.
        
        :return: status of output.
        '''
        # uses lock from sub-call
        return int(self.query_channel('OUTPUT', channel, **kwargs))
    
    @acquire_lock()
    def measure(self, channel):
        '''
        Measure output voltage, current.

        :param channel: output to be measured.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        
        :return: tuple `(voltage, current)`.
        '''
        return (
            float(self.query_channel('OUTPUT_VOLTAGE', channel, no_lock=True).strip()),
            float(self.query_channel('OUTPUT_CURRENT', channel, no_lock=True).strip())
        )

# end class HMP4040


HMP4040.generate_methods()
