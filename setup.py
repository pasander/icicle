from setuptools import setup, find_packages
from sys import version_info

REQUIREMENTS=[
        "wheel>=0.36.2",
        "pyvisa==1.11.3",
        "pyvisa-py==0.5.2",
        "pyserial==3.5",
        "pyusb==1.1.1",
        "minimalmodbus==0.7",
        "click>=8.0.1",
        "paho-mqtt>=1.6.1,<2",
    ]

# Cython changes => need to force particular numpy version
if version_info < (3, 7, 0):
    # Python 3.6 => numpy 1.19.2
    REQUIREMENTS.append("numpy==1.19.2")
else:
    # Python > 3.7 => numpy 1.21+
    REQUIREMENTS.append("numpy>=1.21.0")

setup(
    name='icicle',
    description='Instrument Control Interfaces and Commands Library @ ETHZ',
    author='David Bacher, Simon Koch, Vasilije Perovic, Daniele Ruini.',
    author_email='simon.florian.koch@cern.ch',
    version='1.2.0',
    packages=find_packages(),
    include_package_data=True,
    install_requires=REQUIREMENTS,
    python_requires='>=3.6',
    entry_points={
        'console_scripts': [
            'hmp4040 = icicle.hmp4040_cli:cli',
            'keithley2000 = icicle.keithley2000_cli:cli',
            'keithley2410 = icicle.keithley2410_cli:cli',
            'relay_board = icicle.relay_board_cli:cli',
            'tti = icicle.tti_cli:cli',
            'mp1 = icicle.mp1_cli:cli',
            'instrument_cluster = icicle.instrument_cluster_cli:cli',
            'lauda = icicle.lauda_cli:cli',
            ],
        },
)

