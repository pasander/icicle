﻿relay\_board
============

.. currentmodule:: icicle.relay_board





.. automodule:: icicle.relay_board
   :ignore-module-all:
 
   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      ~RelayBoard
   
   

   
   
   


















RelayBoard
----------

.. autoclass:: RelayBoard
   :members:       
   :undoc-members:    
   :show-inheritance:     

   
   .. automethod:: __init__

   
   

   
   
   





