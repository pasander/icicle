﻿scpi\_instrument
================

.. currentmodule:: icicle.scpi_instrument





.. automodule:: icicle.scpi_instrument
   :ignore-module-all:
 
   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      ~is_in
      ~is_integer
      ~is_numeric
      ~map_to
      ~truthy
      ~verifier_or
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      ~SCPIInstrument
   
   

   
   
   .. rubric:: Exceptions

   .. autosummary::
   
      ~ValidationError
   
   










is\_in
------

.. autofunction:: is_in

is\_integer
-----------

.. autofunction:: is_integer

is\_numeric
-----------

.. autofunction:: is_numeric

map\_to
-------

.. autofunction:: map_to

truthy
------

.. autofunction:: truthy

verifier\_or
------------

.. autofunction:: verifier_or












SCPIInstrument
--------------

.. autoclass:: SCPIInstrument
   :members:       
   :undoc-members:    
   :show-inheritance:     

   
   .. automethod:: __init__

   
   

   
   
   








ValidationError
---------------

.. autofunction:: ValidationError

